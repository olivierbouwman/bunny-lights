var LifxClient = require('node-lifx').Client;
var client = new LifxClient();

var dim = 10;
var lightsOnline = 0;
var l1;
var l2;
var l3;
var l4;
var l5;
var l6;
var l7;
var l8;
var l9;
var l10;
var l11;
var l12;
var l13;
var l14;
var l15;
var l16;
var l17;
var l18;
var l19;
var l20;
var l21;
var l22;
var l23;
var l24;
var l25;
var l26;
var l27;
var l28;
var l29;
var l30;
var l31;
var l32;
var l33;
var l34;
var l35;
var l36;
var l37;
var l38;
var l39;
var l40;
var l41;
var l42;
var l43;
var l44;
var l45;
var l46;
var l47;
var l48;
var l49;
var l50;

var lifxDuration = 500;

var videos;
//var videoPath = '/Users/olivier/Repos/bunny-lights/html5-video/videos/';
var videoPath = '/Users/hp/bunny-lights/html5-video/videos/';

//client.init();
client.init({
	lightOfflineTolerance: 10,
	resendMaxTimes: 0,
	debug: false
});

client.on('message', function(msg, rinfo) {
    if (typeof msg.type === 'string') {
        // Known packages send by the lights as broadcast
        switch (msg.type) {
            case 'echoResponse':
            case 'getOwner':
            case 'stateOwner':
            case 'getGroup':
            case 'getVersion':
            case 'stateGroup':
            case 'getLocation':
            case 'stateLocation':
            case 'stateTemperature':
                // console.log(msg, ' from ' + rinfo.address);
                break;
            default:
                break;
        }
    } else {
        // Unknown message type
        // console.log(msg, ' from ' + rinfo.address);
    }
});

client.on('light-new', function(light) {
    console.log('New light found. ID:' + light.id + ', IP:' + light.address + ':' + light.port);
    light.on(1000);
	light.colorRgb(5, 0, 0, lifxDuration);
	lightsOnline++;
	console.log(lightsOnline + ' lights online');
	if (lightsOnline == 50) {
		console.log('found all lights, stopping discovery');
		client.stopDiscovery();
	}
	setColor(light);
});

client.on('light-online', function(light) {
	lightsOnline++;
	console.log(lightsOnline + ' lights online');
    //console.log('Light back online. ID:' + light.id + ', IP:' + light.address + ':' + light.port);
});

client.on('light-offline', function(light) {
	lightsOnline--;
	console.log(lightsOnline + ' lights online');
    //console.log('Light offline. ID:' + light.id + ', IP:' + light.address + ':' + light.port);
});

client.on('listening', function() {
    var address = client.address();
    console.log(
        'Started LIFX listening on ' +
        address.address + ':' + address.port + '\n'
    );
});


function setColor(light) {
    setTimeout(function() {
        if (light.id == "d073d52126bc" && typeof(l1) != 'undefined') {
            //console.log('setting light: ' + light.id + ' to color: ' + l1.data[0] + '/' + l1.data[1] + '/' + l1.data[2]);
            light.colorRgb(l1.data[0]/dim,  l1.data[1]/dim,  l1.data[2]/dim,  lifxDuration);
			//console.log('-1-');
        } else if (light.id == "d073d5212871" && typeof(l2) != 'undefined') {
            light.colorRgb(l2.data[0]/dim,  l2.data[1]/dim,  l2.data[2]/dim,  lifxDuration);
			//console.log('-2-');
        } else if (light.id == "d073d52131fb" && typeof(l3) != 'undefined') {
            light.colorRgb(l3.data[0]/dim,  l3.data[1]/dim,  l3.data[2]/dim,  lifxDuration);
			//console.log('-3-');
        } else if (light.id == "d073d5217839" && typeof(l4) != 'undefined') {
            light.colorRgb(l4.data[0]/dim,  l4.data[1]/dim,  l4.data[2]/dim,  lifxDuration);
			//console.log('-4-');
        } else if (light.id == "d073d52180d5" && typeof(l5) != 'undefined') {
            light.colorRgb(l5.data[0]/dim,  l5.data[1]/dim,  l5.data[2]/dim,  lifxDuration);
			//console.log('-5-');
        } else if (light.id == "d073d5219acf" && typeof(l6) != 'undefined') {
            light.colorRgb(l6.data[0]/dim,  l6.data[1]/dim,  l6.data[2]/dim,  lifxDuration);
			//console.log('-6-');
        } else if (light.id == "d073d5217ef7" && typeof(l7) != 'undefined') {
            light.colorRgb(l7.data[0]/dim,  l7.data[1]/dim,  l7.data[2]/dim,  lifxDuration);
			//console.log('-7-');
        } else if (light.id == "d073d5217cce" && typeof(l8) != 'undefined') {
            light.colorRgb(l8.data[0]/dim,  l8.data[1]/dim,  l8.data[2]/dim,  lifxDuration);
			//console.log('-8-');
        } else if (light.id == "d073d52187a9" && typeof(l9) != 'undefined') {
            light.colorRgb(l9.data[0]/dim,  l9.data[1]/dim,  l9.data[2]/dim,  lifxDuration);
			//console.log('-9-');
        } else if (light.id == "d073d52154f1" && typeof(l10) != 'undefined') {
            light.colorRgb(l10.data[0]/dim,  l10.data[1]/dim,  l10.data[2]/dim,  lifxDuration);
			//console.log('-10-');
        } else if (light.id == "d073d5212a29" && typeof(l11) != 'undefined') {
            light.colorRgb(l11.data[0]/dim,  l11.data[1]/dim,  l11.data[2]/dim,  lifxDuration);
			//console.log('-11-');
        } else if (light.id == "d073d5217f01" && typeof(l12) != 'undefined') {
            light.colorRgb(l12.data[0]/dim,  l12.data[1]/dim,  l12.data[2]/dim,  lifxDuration);
			//console.log('-12-');
        } else if (light.id == "d073d5213252" && typeof(l13) != 'undefined') {
            light.colorRgb(l13.data[0]/dim,  l13.data[1]/dim,  l13.data[2]/dim,  lifxDuration);
			//console.log('-13-');
        } else if (light.id == "d073d5214941" && typeof(l14) != 'undefined') {
            light.colorRgb(l14.data[0]/dim,  l14.data[1]/dim,  l14.data[2]/dim,  lifxDuration);
			//console.log('-14-');
        } else if (light.id == "d073d5217fac" && typeof(l15) != 'undefined') {
            light.colorRgb(l15.data[0]/dim,  l15.data[1]/dim,  l15.data[2]/dim,  lifxDuration);
			//console.log('-15-');
        } else if (light.id == "d073d52129dc" && typeof(l16) != 'undefined') {
            light.colorRgb(l16.data[0]/dim,  l16.data[1]/dim,  l16.data[2]/dim,  lifxDuration);
			//console.log('-16-');
        } else if (light.id == "d073d5213e44" && typeof(l17) != 'undefined') {
            light.colorRgb(l17.data[0]/dim,  l17.data[1]/dim,  l17.data[2]/dim,  lifxDuration);
			//console.log('-17-');
        } else if (light.id == "d073d521191c" && typeof(l18) != 'undefined') {
            light.colorRgb(l18.data[0]/dim,  l18.data[1]/dim,  l18.data[2]/dim,  lifxDuration);
			//console.log('-18-');
        } else if (light.id == "d073d5218b4b" && typeof(l19) != 'undefined') {
            light.colorRgb(l19.data[0]/dim,  l19.data[1]/dim,  l19.data[2]/dim,  lifxDuration);
			//console.log('-19-');
        } else if (light.id == "d073d5218932" && typeof(l20) != 'undefined') {
            light.colorRgb(l20.data[0]/dim,  l20.data[1]/dim,  l20.data[2]/dim,  lifxDuration);
			//console.log('-20-');
        } else if (light.id == "d073d52154a4" && typeof(l21) != 'undefined') {
            light.colorRgb(l21.data[0]/dim,  l21.data[1]/dim,  l21.data[2]/dim,  lifxDuration);
			//console.log('-21-');
        } else if (light.id == "d073d52148bc" && typeof(l22) != 'undefined') {
            light.colorRgb(l22.data[0]/dim,  l22.data[1]/dim,  l22.data[2]/dim,  lifxDuration);
			//console.log('-22-');
        } else if (light.id == "d073d521549b" && typeof(l23) != 'undefined') {
            light.colorRgb(l23.data[0]/dim,  l23.data[1]/dim,  l23.data[2]/dim,  lifxDuration);
			//console.log('-23-');
        } else if (light.id == "d073d5217c18" && typeof(l24) != 'undefined') {
            light.colorRgb(l24.data[0]/dim,  l24.data[1]/dim,  l24.data[2]/dim,  lifxDuration);
			//console.log('-24-');
        } else if (light.id == "d073d521807c" && typeof(l25) != 'undefined') {
            light.colorRgb(l25.data[0]/dim,  l25.data[1]/dim,  l25.data[2]/dim,  lifxDuration);
			//console.log('-25-');
        } else if (light.id == "d073d52184a2" && typeof(l26) != 'undefined') {
            light.colorRgb(l26.data[0]/dim,  l26.data[1]/dim,  l26.data[2]/dim,  lifxDuration);
			//console.log('-26-');
        } else if (light.id == "d073d5213df6" && typeof(l27) != 'undefined') {
            light.colorRgb(l27.data[0]/dim,  l27.data[1]/dim,  l27.data[2]/dim,  lifxDuration);
			//console.log('-27-');
        } else if (light.id == "d073d5214858" && typeof(l28) != 'undefined') {
            light.colorRgb(l28.data[0]/dim,  l28.data[1]/dim,  l28.data[2]/dim,  lifxDuration);
			//console.log('-28-');
        } else if (light.id == "d073d521502e" && typeof(l29) != 'undefined') {
            light.colorRgb(l29.data[0]/dim,  l29.data[1]/dim,  l29.data[2]/dim,  lifxDuration);
			//console.log('-29-');
        } else if (light.id == "d073d52149a6" && typeof(l30) != 'undefined') {
            light.colorRgb(l30.data[0]/dim,  l30.data[1]/dim,  l30.data[2]/dim,  lifxDuration);
			//console.log('-30-');
        } else if (light.id == "d073d5217b9a" && typeof(l31) != 'undefined') {
            light.colorRgb(l31.data[0]/dim,  l31.data[1]/dim,  l31.data[2]/dim,  lifxDuration);
			//console.log('-31-');
        } else if (light.id == "d073d5214da1" && typeof(l32) != 'undefined') {
            light.colorRgb(l32.data[0]/dim,  l32.data[1]/dim,  l32.data[2]/dim,  lifxDuration);
			//console.log('-32-');
        } else if (light.id == "d073d5218087" && typeof(l33) != 'undefined') {
            light.colorRgb(l33.data[0]/dim,  l33.data[1]/dim,  l33.data[2]/dim,  lifxDuration);
			//console.log('-33-');
        } else if (light.id == "d073d5218139" && typeof(l34) != 'undefined') {
            light.colorRgb(l34.data[0]/dim,  l34.data[1]/dim,  l34.data[2]/dim,  lifxDuration);
			//console.log('-34-');
        } else if (light.id == "d073d5218130" && typeof(l35) != 'undefined') {
            light.colorRgb(l35.data[0]/dim,  l35.data[1]/dim,  l35.data[2]/dim,  lifxDuration);
			//console.log('-35-');
        } else if (light.id == "d073d5214937" && typeof(l36) != 'undefined') {
            light.colorRgb(l36.data[0]/dim,  l36.data[1]/dim,  l36.data[2]/dim,  lifxDuration);
			//console.log('-36-');
        } else if (light.id == "d073d521801f" && typeof(l37) != 'undefined') {
            light.colorRgb(l37.data[0]/dim,  l37.data[1]/dim,  l37.data[2]/dim,  lifxDuration);
			//console.log('-37-');
        } else if (light.id == "d073d5215479" && typeof(l38) != 'undefined') {
            light.colorRgb(l38.data[0]/dim,  l38.data[1]/dim,  l38.data[2]/dim,  lifxDuration);
			//console.log('-38-');
        } else if (light.id == "d073d521542d" && typeof(l39) != 'undefined') {
            light.colorRgb(l39.data[0]/dim,  l39.data[1]/dim,  l39.data[2]/dim,  lifxDuration);
			//console.log('-39-');
        } else if (light.id == "d073d5217863" && typeof(l40) != 'undefined') {
            light.colorRgb(l40.data[0]/dim,  l40.data[1]/dim,  l40.data[2]/dim,  lifxDuration);
			//console.log('-40-');
        } else if (light.id == "d073d521781b" && typeof(l41) != 'undefined') {
            light.colorRgb(l41.data[0]/dim,  l41.data[1]/dim,  l41.data[2]/dim,  lifxDuration);
			//console.log('-41-');
        } else if (light.id == "d073d5217969" && typeof(l42) != 'undefined') {
            light.colorRgb(l42.data[0]/dim,  l42.data[1]/dim,  l42.data[2]/dim,  lifxDuration);
			//console.log('-42-');
        } else if (light.id == "d073d5217890" && typeof(l43) != 'undefined') {
            light.colorRgb(l43.data[0]/dim,  l43.data[1]/dim,  l43.data[2]/dim,  lifxDuration);
			//console.log('-43-');
        } else if (light.id == "d073d52151d5" && typeof(l43) != 'undefined') {
            light.colorRgb(l44.data[0]/dim,  l44.data[1]/dim,  l44.data[2]/dim,  lifxDuration);
			//console.log('-44-'); //not found
        } else if (light.id == "d073d520e30c" && typeof(l45) != 'undefined') {
            light.colorRgb(l45.data[0]/dim,  l45.data[1]/dim,  l45.data[2]/dim,  lifxDuration);
			//console.log('-45-'); //not found
        } else if (light.id == "d073d5217c9d" && typeof(l46) != 'undefined') {
            light.colorRgb(l46.data[0]/dim,  l46.data[1]/dim,  l46.data[2]/dim,  lifxDuration);
			//console.log('-46-');
        } else if (light.id == "d073d5218952" && typeof(l47) != 'undefined') {
            light.colorRgb(l47.data[0]/dim,  l47.data[1]/dim,  l47.data[2]/dim,  lifxDuration);
			//console.log('-47-');
        } else if (light.id == "d073d5214ab4" && typeof(l48) != 'undefined') {
            light.colorRgb(l48.data[0]/dim,  l48.data[1]/dim,  l48.data[2]/dim,  lifxDuration);
			//console.log('-48-');
        } else if (light.id == "d073d5217ead" && typeof(l49) != 'undefined') {
            light.colorRgb(l49.data[0]/dim,  l49.data[1]/dim,  l49.data[2]/dim,  lifxDuration);
			//console.log('-49-');
        } else if (light.id == "d073d5215176" && typeof(l50) != 'undefined') {
            light.colorRgb(l50.data[0]/dim,  l50.data[1]/dim,  l50.data[2]/dim,  lifxDuration);
			//console.log('-50-');
        } else {
			//console.log('Unknown light found. ID:' + light.id);
		}
		
        setColor(light);
    }, (250)); //50ms is maximum (Maximum recommended message transmit rate to a device: 20 per second)
}

function canvasVideo() {

    if (document.createElement('canvas').getContext == undefined) {
        var msg = "<div style='padding:5px;margin:5px;border:1px solid #000;text-align:center;'>Sorry, you need browser that supports HTML5 to see this.</div>";
        document.getElementById('canvasVideo').parentNode.innerHTML = msg;
        document.getElementById('canvasVideoSmall').parentNode.innerHTML = msg;
        return;
    }

    var $ = jQuery,
        video = document.getElementById('canvasVideo'),
        canvas = document.getElementById('canvasVideoSmall'),
        canvasContext = canvas.getContext('2d'),
        w = canvas.width,
        h = canvas.height;
        // console.log('width: ' + w);
        // console.log('height: ' + h);

        video.addEventListener('play', function() {
            loadRandomVideoFromDirectory(videoPath); //redo on every video play so I can add remove videos without stopping execution
            window.setInterval(function(t) {
                if (video.readyState > 0) {
                    if (video.currentTime < 5) {
                        // video.currentTime = video.duration - 5; //testing
                        video.currentTime = video.currentTime = 20; //skip intro
                    }
                    clearInterval(t);
                }
            }, 500);
			
            drawFrame();
        }, false);

    video.addEventListener('ended', function() {
        video.pause();
        randomVideo = 'videos/' + videos[Math.floor(Math.random() * videos.length)];
        console.log('next video to play: ' + randomVideo);
        this.src = randomVideo;
        video.load();
        video.play();
    }, false);

    function drawFrame() {
        canvasContext.drawImage(video, 0, 0, w, h);

        l1 = canvasContext.getImageData(5, 1, 1, 1);

        l2 = canvasContext.getImageData(4, 2, 1, 1);
        l3 = canvasContext.getImageData(5, 2, 1, 1);
        l4 = canvasContext.getImageData(6, 2, 1, 1);
        l5 = canvasContext.getImageData(7, 2, 1, 1);

        l6 = canvasContext.getImageData(4, 3, 1, 1);
        l7 = canvasContext.getImageData(5, 3, 1, 1);
        l8 = canvasContext.getImageData(6, 3, 1, 1);
        l9 = canvasContext.getImageData(7, 3, 1, 1);
        l10 = canvasContext.getImageData(8, 3, 1, 1);

        l11 = canvasContext.getImageData(3, 4, 1, 1);
        l12 = canvasContext.getImageData(4, 4, 1, 1);
        l13 = canvasContext.getImageData(5, 4, 1, 1);
        l14 = canvasContext.getImageData(6, 4, 1, 1);
        l15 = canvasContext.getImageData(7, 4, 1, 1);
        l16 = canvasContext.getImageData(8, 4, 1, 1);

        l17 = canvasContext.getImageData(2, 5, 1, 1);
        l18 = canvasContext.getImageData(3, 5, 1, 1);
        l19 = canvasContext.getImageData(4, 5, 1, 1);
        l20 = canvasContext.getImageData(5, 5, 1, 1);
        l21 = canvasContext.getImageData(6, 5, 1, 1);
        l22 = canvasContext.getImageData(7, 5, 1, 1);
        l23 = canvasContext.getImageData(8, 5, 1, 1);

        l24 = canvasContext.getImageData(2, 6, 1, 1);
        l25 = canvasContext.getImageData(3, 6, 1, 1);
        l26 = canvasContext.getImageData(4, 6, 1, 1);
        l27 = canvasContext.getImageData(5, 6, 1, 1);
        l28 = canvasContext.getImageData(6, 6, 1, 1);
        l29 = canvasContext.getImageData(7, 6, 1, 1);
        l30 = canvasContext.getImageData(8, 6, 1, 1);
        l31 = canvasContext.getImageData(9, 6, 1, 1);

        l32 = canvasContext.getImageData(1, 7, 1, 1);
        l33 = canvasContext.getImageData(2, 7, 1, 1);
        l34 = canvasContext.getImageData(3, 7, 1, 1);
        l35 = canvasContext.getImageData(4, 7, 1, 1);
        l36 = canvasContext.getImageData(5, 7, 1, 1);
        l37 = canvasContext.getImageData(6, 7, 1, 1);
        l38 = canvasContext.getImageData(7, 7, 1, 1);
        l39 = canvasContext.getImageData(8, 7, 1, 1);
        l40 = canvasContext.getImageData(9, 7, 1, 1);

        l41 = canvasContext.getImageData(1, 8, 1, 1);
        l42 = canvasContext.getImageData(2, 8, 1, 1);
        l43 = canvasContext.getImageData(3, 8, 1, 1);
        l44 = canvasContext.getImageData(4, 8, 1, 1);
        l45 = canvasContext.getImageData(5, 8, 1, 1);
        l46 = canvasContext.getImageData(6, 8, 1, 1);
        l47 = canvasContext.getImageData(7, 8, 1, 1);
        l48 = canvasContext.getImageData(8, 8, 1, 1);
        l49 = canvasContext.getImageData(9, 8, 1, 1);
        l50 = canvasContext.getImageData(10, 8, 1, 1);

        setTimeout(function() {
            drawFrame();
        }, 250); //40fps=25 (1000/40)
    }
}

function loadRandomVideoFromDirectory(path) {
    var fs = require('fs');
    fs.readdir(path, function(err, items) {
        videos = items;
    });
}

(function($) {
    $(document).ready(function() {
        canvasVideo();
    });
})(jQuery);
